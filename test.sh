#!/bin/sh

pip install -q -U -r requirements_test.txt || exit 1

py.test --doctest-module --pep8 hcs_utils "$@" | tee test.last
